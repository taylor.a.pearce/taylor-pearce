document.addEventListener("DOMContentLoaded", function () {
    // Data for skills
    const skills = [
        { name: "Python", icon: "assets/images/python.png" },
        { name: "JavaScript", icon: "assets/images/javascript.png" },
        { name: "Swift", icon: "assets/images/swift2.webp" },
        { name: "SwiftUI", icon: "assets/images/swiftui.png" },
        { name: "SwiftData", icon: "assets/images/swiftdata.png" },
        { name: "React.js", icon: "assets/images/react.svg" },
        { name: "Django", icon: "assets/images/django.png" },
        { name: "PostgreSQL", icon: "assets/images/postgresql.svg" },
        { name: "SQL", icon: "assets/images/sql.png" },
        { name: "FastAPI", icon: "assets/images/fastapi.svg" },
        { name: "HTML5", icon: "assets/images/html5.svg" },
        { name: "CSS", icon: "assets/images/css.svg" },
        { name: "Docker", icon: "assets/images/docker.png" },
        { name: "AutoCAD Civil 3D", icon: "assets/images/autocad.png" },
        { name: "Bentley MicroStation", icon: "assets/images/microstation.jpg" },
    ];

    // Data for projects
    const projects = [
        {
            title: "Trivia Titans",
            description: `Enter the world of Trivia Titans, a Full Stack Web Application that reflects my proficiency in ReactJS, FastAPI, PostgreSQL, Python, JavaScript, HTML, CSS, and Docker. As a Full Stack Developer, I played a pivotal role in creating this engaging trivia game designed for solo play. In this project, I meticulously constructed a robust questions database and table using PostgreSQL, leveraging current AI technology to curate and input hundreds of questions in SQL format. The implementation of FastAPI CRUD endpoints for the trivia questions database table further solidified the application's functionality. Additionally, I focused on delivering a dynamic frontend design, utilizing CSS and media queries to ensure seamless adaptability to various screen sizes. Trivia Titans is a reflection of my commitment to crafting engaging and technically sound web applications.`,
            image: "assets/images/projects/trivia.png",
            repoUrl: "https://gitlab.com/ice-climbers/trivia-titans"
        },
        {
            title: "AutoSphere",
            description: `AutoSphere represents my work as a Full Stack Developer, showcasing proficiency in ReactJS, Django, Python, JavaScript, HTML, CSS, and Bootstrap. This web application, designed for automobile dealership management, operates on a microservice architecture. My role involved crafting an interactive platform to handle inventory, and sales efficiently. The use of Docker further streamlined the development process, emphasizing simplicity and scalability. AutoSphere stands as a testament to my skills in designing and engineering solutions for the intricacies of automotive sales and service facilities.`,
            image: "assets/images/projects/AutoSphere.png",
            repoUrl: "https://gitlab.com/taylor.a.pearce/autosphere"
        },
    ];

    // Function to dynamically populate skills
    function populateSkills() {
        const skillGrid = document.querySelector('.skill-grid');
        skills.forEach(skill => {
            const skillItem = document.createElement('div');
            skillItem.classList.add('skill');

            const skillImage = document.createElement('img');
            skillImage.src = skill.icon;
            skillImage.style.height = '100px';

            const skillName = document.createElement('h4');
            skillName.textContent = skill.name;

            skillItem.appendChild(skillImage);
            skillItem.appendChild(skillName);
            skillGrid.appendChild(skillItem);
        });
    }

    // Function to dynamically populate projects
    function populateProjects() {
        const projectGallery = document.getElementById('projectGallery');
        projects.forEach(project => {
            const projectItem = document.createElement('div');
            projectItem.classList.add('project');

            const projectImage = document.createElement('img');
            projectImage.src = project.image;
            projectImage.alt = project.title;

            const projectTitle = document.createElement('h3');
            projectTitle.classList.add('project-title');
            projectTitle.textContent = project.title;

            const projectDescription = document.createElement('p');
            projectDescription.textContent = project.description;

            const repoButton = document.createElement('button');
            repoButton.textContent = 'Repo Link';
            repoButton.classList.add('project-button');
            repoButton.addEventListener('click', function () {
                window.location.href = project.repoUrl;
            });

            projectItem.appendChild(projectImage);
            projectItem.appendChild(projectTitle);
            projectItem.appendChild(repoButton);
            projectItem.appendChild(projectDescription);


            projectGallery.appendChild(projectItem);
        });
    }

    // Call functions to populate skills and projects
    populateSkills();
    populateProjects();

    // Smooth scrolling for anchor links
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();

            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });
});

document.addEventListener("DOMContentLoaded", function () {
    const menuIcon = document.querySelector('.menu-icon');
    const mobileNav = document.querySelector('.mobile-nav');

    menuIcon.addEventListener('click', function () {
        mobileNav.style.display = (mobileNav.style.display === 'block') ? 'none' : 'block';
    });
});
